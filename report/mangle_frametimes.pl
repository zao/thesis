#!/usr/bin/env perl

use strict;
use warnings;
use v5.10;

use Data::Dumper;
use JSON;
use Path::Tiny qw( path );

my $json = JSON->new->utf8;
my $json_text = path('bench.json')->slurp;
my $root = $json->decode($json_text);

{
	my $fh = path('frametime-meshes.dat')->openw;
	say $fh "count\tframetime";
	for my $p (@{$root->{meshes}}) {
		say $fh "$p->{x}\t$p->{y}";
	}
}

{
	my $fh = path('frametime-impostors.dat')->openw;
	say $fh "count\tframetime";
	for my $p (@{$root->{impostors}}) {
		say $fh "$p->{x}\t$p->{y}";
	}
}

{
	my $fh = path('frametime-mix.dat')->openw;
	say $fh "count\tframetime";
	for my $p (@{$root->{mix}}) {
		say $fh "$p->{x}\t$p->{y}";
	}
}
