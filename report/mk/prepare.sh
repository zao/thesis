#!/usr/bin/env bash
ROOTFILES="alphaurl.bst draftwatermark.sty everypage.sty plainurl.bst refs.bib thesis.tex thesis_report.cls"
CHAPTERS="accomplishment.tex acknowledgements.tex conclusions.tex in_depth_study.tex introduction.tex problem_description.tex results.tex"

for n in ${ROOTFILES}; do ln -fs ../$n; done
mkdir chapters &>/dev/null
pushd chapters &>/dev/null
for n in ${CHAPTERS}; do ln -fs ../../chapters/$n; done
popd &>/dev/null
