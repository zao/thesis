Build instructions
==================
In the `mk` directory there is a GNU Makefile.

* `make prepare` to link in all source files and styles
* `make distclean` to clean out the build directory completely
* `make` to build
* `make clean` to clean
