\documentclass[aspectratio=1610,english]{beamer}
\usepackage{fontspec}
\usepackage{tikz}
\usepackage{enumitem}
\usepackage{listings}

\usepackage{pgfplots}
\usepgfplotslibrary{units}
\usepackage[space-before-unit,range-units = repeat]{siunitx}

\pgfplotsset{compat=1.9}

\usetheme{Berkeley}
% \usecolortheme{solarized}
\title[Rendering Dynamic Crowds with Level-of-detail]{High Performance Rendering of Dynamic Crowds with Smooth Level-of-detail}
\author{Lars Viklund}
\institute{Umeå University}
\date{June 6, 2015}

\begin{document}

\begin{frame}
\titlepage
\end{frame}

\section{Introduction}
\subsection{Talk structure}
\begin{frame}
\frametitle{Outline of talk}
\begin{itemize}
\item[] Mention project background and determine the performance problems to be solved.
\item[] Touching on needed concepts of computer graphics.
\item[] Outline the techniques from the two articles used to improve runtime performance.
\item[] Conclude with measurements of their impact on the project and their costs.
\end{itemize}
\end{frame}

\subsection{Project background}
\begin{frame}
\frametitle{Project background and purpose}
\begin{itemize}
\item[] Armagent AB has a game prototype project.
\item[] Real-time tactical game, like the Total War series.
\item[] Large numbers of soldiers on virtual battlefields.
\item[] Soldiers move and act individually with several animations.
\end{itemize}
\end{frame}

\subsection{Problems}
% NOTE(zao): Outline problems that need a solution in existing codebase.
\begin{frame}
\frametitle{Problems in game application}
\begin{itemize}
\item[] Bottleneck in animating and drawing the soldiers.
\item[] Computing the animation matrices for a soldier takes time.
\item[] Saturates processor as unit count increases.
\item[] Tons of geometry also saturate the graphics card.

\item[] Techniques are needed to reduce processing cost.
\end{itemize}
\end{frame}

\section{Computer graphics}
% NOTE(zao): Relevant concepts of computer graphics or GPUs.
\begin{frame}
\frametitle{Introduction to Computer Graphics}
\begin{itemize}[label={}]
\item Computer graphics is data oriented.
\item Tons of data, suitable for long parallel pipelines.
\item Bundles of geometry turns into fragments which turn into images.
\item Rapidly and constantly build and display images (frames).
\item Usually 60 frames per second.
\item One every 16.67 milliseconds.
\end{itemize}
\end{frame}

% NOTE(zao): Shaders
\subsection{GPUs and shaders}
\begin{frame}[fragile]
\frametitle{Graphics Processing Units and shaders}
A GPU is a hardware device designed for fast graphics.

It runs stream processing programs called shaders.

Shaders process individual pieces of data in parallel.

\begin{lstlisting}[language=C,basicstyle=\ttfamily,keywordstyle=\color{blue}]
struct Input {
    float3 color : COLOR0;
    float2 texcoord : TEXCOORD0;
};

Texture2D diffuse;

float4 pixel_shader_main(Input input) {
    float4 c = diffuse.Sample(input.texcoord);
    c.rgb *= input.color;
    return c;
}
\end{lstlisting}
\end{frame}

\subsection{Representation}
% NOTE(zao): Trisoups, vertex streams, primitives.
\begin{frame}
\frametitle{Data representation}
\begin{itemize}[label={}]
\item GPUs prefer structured data of predetermined shape.
\item Meshes of triangles like $\left\{v_1, v_2, v_3\right\}, \left\{v_4, v_5, v_6\right\}$.
\item Each vertex has typed attribute sets:
	\begin{itemize}[label={}]
	\item position, colour, normal, bone assignments, bone weights, etc.
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Vertex and pixel shaders}
Vertex shader operates on vertices used in triangles.
\begin{itemize}[label={}]
\item Transforms vertex attributes from original representation with matrices.
\item Outputs a screenspace position and attributes to be interpolated across triangle.
\end{itemize}

Pixel shader operates on screen pixels covered by the geometry.
\begin{itemize}[label={}]
\item Computes the colour and translucency of the pixel.
\item Uses vectors and positions from camera and pixel.
\end{itemize}
\end{frame}

\subsection{Skinning}
\begin{frame}
% TODO(zao): Add equation here instead?
\frametitle{Vertex blend skinning}
\begin{itemize}
\item Blend skinning is a technique to deform geometry.
\item Skeleton of hierarchical bones/joints is rigged inside mesh.
\item Vertices are associated with a subset of joints with weights.
\item Cheap and intuitive manipulation, just adjust stick figure.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Math of blend skinning}
\begin{itemize}
\item Two poses involved, bind pose and animation pose.
\item Use base pose to transform vertex from object space to bone space.
\item Use animation pose to transform back to object space.
\item Result is the vertex moving with the bone.
\item Weights blend the vertex between multiple influencing bones.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Applied blend skinning}
\begin{itemize}
\item Matrices for pose computed on CPU.
\item Applying blend skinning is cheap on GPU.
\item Requires access to computed bind/bone matrices.
\item Usually uploaded as buffer data for each draw call.
\end{itemize}
\end{frame}

\subsection{Invocation}
% TODO(zao): Draw call, severe limiter for scaling.
\begin{frame}
\frametitle{Actually drawing things}
\begin{itemize}
\item Several calls to set up state needed.
\item Bind data buffers, shader programs, parameters, images\ldots
\item Finally issue draw call using all that state.
\end{itemize}
\begin{itemize}
\item CPU budget limits draw count for frame.
\item Doing same work in fewer calls needed for scaling.
\end{itemize}
\end{frame}

\subsection{Instancing}
% TODO(zao): Importance of instancing, amplify vertex streams.
\begin{frame}
\frametitle{Geometry instancing}
\begin{itemize}
\item Instancing generates the Cartesian product of per-vertex and per-instance data.
\item As if drawing each mesh in turn given a piece of per-instance data each.
\item If meshes are similar enough, N calls can be turned into a single instanced call.
\end{itemize}

\begin{equation} V = \begin{pmatrix}
p_1 & p_2 & p_3 \\
n_1 & n_2 & n_3
\end{pmatrix}
\end{equation}
\begin{equation} I = \left\lbrace
\begin{pmatrix} t_1 \\ c_1 \end{pmatrix},
\begin{pmatrix} t_2 \\ c_3 \end{pmatrix},
\begin{pmatrix} t_3 \\ c_2 \end{pmatrix}
\right\rbrace
\end{equation}

\begin{equation}\textrm{Instances}(V,I) = \left\lbrace
\begin{pmatrix} p_1 & p_2 & p_3 \\ n_1 & n_2 & n_3 \\ t_1 & t_1 & t_1 \\ c_1 & c_1 & c_1 \end{pmatrix},\begin{pmatrix} p_1 & p_2 & p_3 \\ n_1 & n_2 & n_3 \\ t_2 & t_2 & t_2 \\ c_2 & c_2 & c_2 \end{pmatrix}, 
\begin{pmatrix} p_1 & p_2 & p_3 \\ n_1 & n_2 & n_3 \\ t_3 & t_3 & t_3 \\ c_3 & c_3 & c_3 \end{pmatrix}
\right\rbrace \end{equation}
\end{frame}

\subsection{Level-of-detail}
% TODO(zao): Level of detail.
\begin{frame}
\frametitle{Level of detail}
\begin{itemize}
\item Level of detail is a wide field.
\item Covers the use of dynamically simplified or replacement meshes.
\item Also deals with alternative representations.
\item Used for less important or distant entities.
\end{itemize}
\end{frame}

\section{Solution}
\subsection{Method}
% TODO(zao): Literature study, testbed, and selection of techniques.
\begin{frame}
\frametitle{Implementation as prototype testbed}
\begin{itemize}
\item For ease of implementation and ability to benchmark, a testbed was created.
\item A platform for placing and animating soldiers on a large terrain.
\item Implemented as a standalone project outside of game.
\item Offers known performance to calibrate results against, as static workload exists.
\item Free from competing dynamic workloads like AI and physics.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Testbed LoD}
Baseline implementation with individual draw calls.

Has rudimentary culling for representative costs.

Switches between LoD representations by distance.
\begin{figure}
\begin{tabular}{ll}
Distance & Representation \\
\hline
Near & Mesh (many triangles) \\
Medium & Mesh (few triangles) \\
Far & Impostor
\end{tabular}
\end{figure}

	\begin{tikzpicture}[remember picture,overlay]
		\node[xshift=-2cm] at (current page.east) {
			\includegraphics[height=0.9\paperheight]{../report/rus-mesh.png}
		};
	\end{tikzpicture}
\end{frame}

\subsection{Literature}
\begin{frame}
\frametitle{Literature study}
\begin{itemize}
\item Five papers covered in report.
\item Outline two implemented papers.
\end{itemize}
\begin{itemize}[label=$\star$]
\item Animated Crowd Rendering -- Bryan Dudash, NVIDIA Corporation
\item Geopostors -- Dobbyn et al, Trinity College Dublin
\end{itemize}
\end{frame}

% TODO(zao): Outline Stadium paper.
\begin{frame}
\frametitle{Animated Crowd Rendering}
\begin{itemize}
\item Quantize animations into keyframes.
\item Vertex blend skinning by precomputing matrices.
\item Each bone in each frame has $4 \times 4$ matrix.
\item Stored in texture as three 4-element vectors.
\item Fourth vector implied as $(0, 0, 0, 1)$.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Drawing animated crowds}
Instance data is
\begin{itemize}
\item offset into texture to find data for animation frame;
\item world orientation and position,
\item colour for individualisation tint.
\end{itemize}

Runtime data is
\begin{itemize}
\item matrix textures,
\item colour texture for geometry,
\item mask texture where to apply tint.
\end{itemize}

Characters are holding items, instanced by object type.

Together with tinting breaks conformity.
\end{frame}

% TODO(zao): Outline Geopostors.
\begin{frame}
\frametitle{Geopostors -- animated impostors}
\begin{itemize}
\item Has animated characters wandering around.

\item Replaces non-close meshes with impostors.
\item Impostors are a simple flat geometry.
\item Geometry textured with image of mesh.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Impostor generation}
\begin{itemize}
\item Impostors pre-generated offline.
\item Draws mesh from all viewpoints in a set into an image sheet.

\item Animations are quantised.
\item Each frame a separate impostor sheet is generated.
\item Drawing gathers impostors by current animation frame.
\item Assigns viewpoint image based on closest viewpoint.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Impostor rendering}
Drawn using instances.
Instance data is
\begin{itemize}
\item impostor sheet region,
\item world orientation and position,
\item parameters for individualisation.
\end{itemize}

Runtime data is
\begin{itemize}
\item impostor sheets with base colour,
\item impostor sheets with vertex normals,
\item impostor sheets with material groups.
\end{itemize}

Normals for lighting computations.

The material acts as a key into a palette of clothing and skin colours.

Breaks up similarity.
\end{frame}

\subsection{Technique implementation}
% TODO(zao): Describe changes to stadium paper.
\begin{frame}
\frametitle{Adaptations of NVIDIA paper}
Ignoring all forms of individualisation.

Soldiers are very similiar by design.

Extend single animation technique to multiple animations.
\begin{itemize}
\item Repeat the offline computations for each animation.
\item Drawing is separately instanced per animation.
\end{itemize}
\end{frame}

% TODO(zao): Describe changes to geopostors.
\begin{frame}
\frametitle{Adaptation of Dublin paper}
Pack a whole animation into a single sheet.

Still several sheets as there are several animations.

One instanced draw call per animation.

No normal maps, no individualisation.

Offline tool generates and compresses all image sheets.

	\begin{tikzpicture}[remember picture,overlay]
		\node[xshift=-3cm, yshift=-0.5cm] at (current page.east) {
			\includegraphics[height=0.7\paperheight]{../report/rus-walk.png}
		};
	\end{tikzpicture}
\end{frame}

\section{Results}
\subsection{Measurements}
% TODO(zao): Graphs needed to illustrate complex characteristics.
\begin{frame}
\frametitle{Evaluation measurements}
Primary measurement is frame time.

Short frame times suitable for running parameter sweeps.

Keep some parameters fixed, see how frame time varies.

Plot effect of varying the parameter.
\end{frame}

{
\setbeamertemplate{navigation symbols}{}
% NOTE(zao): Show 10k soldiers, demonstrate the vast scale.
\begin{frame} %[plain]
	\frametitle{Ten thousand soldiers}
	\begin{figure}
	\includegraphics[width=\textwidth]{overview-grey.png}
	\end{figure}
\end{frame}
}

% NOTE(zao): Performance figures:
\pgfplotstableread{../report/frametime-meshes.dat}{\frametimemeshes}
\pgfplotstableread{../report/frametime-impostors.dat}{\frametimeimpostors}
\pgfplotstableread{../report/frametime-mix.dat}{\frametimemix}
\pgfplotstableread{../report/frametime-singles.dat}{\frametimesingles}

\pgfplotstableread{../report/cutoff-2000.dat}{\cutofftwok}
\pgfplotstableread{../report/cutoff-4000.dat}{\cutofffourk}
\pgfplotstableread{../report/cutoff-6000.dat}{\cutoffsixk}
\pgfplotstableread{../report/cutoff-8000.dat}{\cutoffeightk}
\pgfplotstableread{../report/cutoff-10000.dat}{\cutofftenk}

% NOTE(zao): * Mix of detail levels
\begin{frame}
\frametitle{Detail level mixes}

\begin{figure}
\begin{tikzpicture}
	\begin{axis}[
		legend pos=north west,
		xlabel=number of entities,
		ylabel=frametime,
		unit markings=parenthesis,
		scaled x ticks=false,
		x tick label style={/pgf/number format/fixed},
		y tick label style={/pgf/number format/fixed},
		y unit=s, change y base=true, y SI prefix=milli]
	\addplot+[x={count}, y={frametime},mark=x] table {\frametimemeshes};
	\addlegendentry{Only meshes}

	\addplot+[x={count}, y={frametime},mark=*] table {\frametimemix};
	\addlegendentry{Distance mix}

	\addplot+[x={count}, y={frametime},mark=square] table {\frametimeimpostors};
	\addlegendentry{Only impostors}
	\end{axis}
\end{tikzpicture}
\caption{Time per frame, varying count of entities and the kind of drawable}
\label{scenario:linear}
\end{figure}
% TODO(zao): Key takeaways.
\end{frame}

% TODO(zao): * Varying cutoff distance.
\begin{frame}
\frametitle{Varying cutoff distances}
\begin{figure}
\begin{tikzpicture}
	\begin{axis}[
		legend pos=north west,
		xlabel=transition distance,
		ylabel=frametime,
		unit markings=parenthesis,
		scaled x ticks=false,
		x tick label style={/pgf/number format/fixed},
		x unit=m,
		y tick label style={/pgf/number format/fixed},
		y unit=s, change y base=true, y SI prefix=milli]	
	\addplot+[x={distance}, y={frametime}] table {\cutofftenk};
	\addlegendentry{10000 entities}
	\addplot+[x={distance}, y={frametime}] table {\cutoffeightk};
	\addlegendentry{8000 entities}
	\addplot+[x={distance}, y={frametime}] table {\cutoffsixk};
	\addlegendentry{6000 entities}	
	\addplot+[x={distance}, y={frametime}] table {\cutofffourk};
	\addlegendentry{4000 entities}
	\addplot+[x={distance}, y={frametime}] table {\cutofftwok};
	\addlegendentry{2000 entities}

	\end{axis}
\end{tikzpicture}
\caption{Time per frame, varying the cutoff for Level of Detail given different entity counts}
\label{scenario:cutoff}
\end{figure}
% TODO(zao): Key takeaways.
\end{frame}

\begin{frame}
\frametitle{Instancing impact}
\begin{figure}
\begin{tikzpicture}
	\begin{axis}[
		legend pos=north west,
		xlabel=number of entities,
		ylabel=frametime,
		unit markings=parenthesis,
		scaled x ticks=false,
		x tick label style={/pgf/number format/fixed},
		y tick label style={/pgf/number format/fixed},
		y unit=s, change y base=true, y SI prefix=milli]
	\addplot+[x={count}, y={frametime},mark=x] table {\frametimemeshes};
	\addlegendentry{Instanced meshes}

	\addplot+[x={count}, y={frametime}] table {\frametimesingles};
	\addlegendentry{Separate meshes}
	\end{axis}
\end{tikzpicture}
\caption{Time per frame, varying count of entities and use of instancing}
\label{scenario:instancing}
\end{figure}
\end{frame}

\subsection{Conclusions and future work}
% NOTE(zao): Restate conclusions from report.
\begin{frame}
\frametitle{Conclusions from study and implementation}
It is feasible to have simulations with several thousands of characters.

Many knobs to tune performance for minimal and recommended configurations.

Leaves time for game code, other computing and drawing.
\end{frame}

% TODO(zao): Cover future work.
\begin{frame}
\frametitle{Possible future work}
This is still a testbed, with biases and caveats.

Actual implementation into game may change and vary.

Future seems bright, given the worst case cases.

Individualisation may be worth it for eye-catching scenarios.
\end{frame}

\begin{frame}
\frametitle{Thanks and acknowledgements}
Thank you to:
\begin{itemize}
\item my current coworkers for encouraging me to complete this;
\item Armagent AB for letting me work on their precious game;
\item Benjamin Nauck for feedback on report drafts;
\item everyone I've discussed everything with.
\end{itemize}
\end{frame}

\subsection{Questions}
\begin{frame}
\frametitle{Bring forth the feedback and questions}
This page intentionally left non-blank.
\end{frame}

\end{document}